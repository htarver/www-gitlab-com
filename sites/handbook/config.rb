#--------------------------------------
# Monorepo-related configuration
#--------------------------------------

monorepo_root = File.expand_path('../..', __dir__)

# hack around relative requires elsewhere in the shared code by adding the monorepo root to the load path
$LOAD_PATH.unshift(monorepo_root)

set :data_dir, "#{monorepo_root}/data"
set :helpers_dir, "../../helpers" # This has to be relative, because Middleman's ExternalHelpers#after_configuration uses File.join(app.root, ...)

require_relative '../../extensions/monorepo.rb'
activate :monorepo do |monorepo|
  monorepo.site = 'handbook'
end

#--------------------------------------
# End of Monorepo-related configuration
#--------------------------------------

require 'extensions/only_debugged_resources'
require 'extensions/partial_build_handbook'
require 'extensions/proxy_server_information'
require 'extensions/open_graph'
require "lib/homepage"
require "lib/team"
require 'lib/mermaid'
require 'lib/plantuml'
require 'lib/code_owners'
require 'generators/prodops_direction'

#----------------------------------------------------------
# Global config (not specific to development or build mode)
#----------------------------------------------------------

# Settings
set :haml, { format: :xhtml }
set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Paths with custom per-page overrides
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# Don't render or include the following into the sitemap
ignore '**/.gitkeep'

# Extensions
activate :syntax, line_numbers: false
if ENV['MIDDLEMAN_DEBUG_RESOURCE_REGEX']
  ::Middleman::Extensions.register(:only_debugged_resources, OnlyDebuggedResources)
  activate :only_debugged_resources
end
activate :partial_build_handbook # Handle splitting of files across CI jobs
activate :open_graph # Add open graph helpers and utilities

#----------------------------------------------------------
# End global config (not specific to development or build mode)
#----------------------------------------------------------

# Development-specific config
configure :development do
  # There is no root index.html in handbook sub-site, so redirect root to handbook/index.html
  redirect "index.html", to: "/handbook/index.html"

  # Reload the browser automatically whenever files change
  activate :livereload unless ENV['ENABLE_LIVERELOAD'] != '1'

  # External Pipeline
  unless ENV['SKIP_EXTERNAL_PIPELINE']
    # NOTE: if we use the external pipeline, we set the Middleman port to 7654,
    # which the webpack devServer config relies on in webpack.config.js in the project root.
    # We set up webpack devServer on port 4567 to keep the local dev commands the same,
    # but it proxies to 7654 for everything other than the webpack assets.
    # See doc/webpack.md for more information.
    set(:port, 7654)
    # This Middleman extension logs a custom message about the Webpack proxy port
    activate :proxy_server_information
    # NOTE: This only applies to 'development' mode.  For local builds, use the `rake build:*` tasks
    # NOTE: This runs webpack and makes assets available during local development
    activate :external_pipeline,
             name: :webpack,
             command: "cd #{monorepo_root} && yarn run start-webpack",
             source: "#{monorepo_root}/tmp/dist",
             latency: 3
  end

  activate :autoprefixer do |config|
    config.browsers = ['last 2 versions', 'Explorer >= 9']
  end
end

# Build-specific configuration
configure :build do
  Kramdown::Converter::PlantUmlHtmlWrapper.plantuml_setup

  # Mermaid diagrams don't render without line breaks
  activate :minify_html, preserve_line_breaks: true
end
